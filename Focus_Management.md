# Focus Management

## 1. What is Deep Work

### **Q1. What is Deep Work?**

Deep work is the work that we do without distraction by being focused and in an efficient manner. That work can be demanding and urgent. But it produces high-quality output.

## 2. The optimal duration for deep work

Saw the Video

## 3. Are deadlines good for productivity?

Saw the Video

## 4. Summary of Deep Work Book

Saw the Video

### **Q2. Paraphrase all the ideas in the above videos and this one in detail.**
The narrator in the video talks about a book called "DEEP WORK" by Cal Newport. He gives a brief of what is there inside the book and what Cal Newport wants to say. The narrator describes the deep work in the following manner - 
* Deep Work is described as Professional Activities performed in Distraction Free Environment which pushes Cognitive skills and Create new values, Improves the skills, and its very hard to replicate for others.

* Deep work enhances the Myline level around Braincell to make it work better.

* In today's world, Deep Work has become increasingly valuable and increasingly rare.

* There are three deep work strategies that we can apply to our professional life -
    * Schedule Distractions
    * Deep Work Ritual
    * Evening Shutdown

### **Q3. How can you implement the principles in your day-to-day life?**

* I can make habits to schedule deep work at a particular time.
* I can make better sleeping habits to improve my memorizing power.
* I can avoid any kind of distraction while doing deep work.
* I can do exercise and meditation to improve my focus power.

## 5. Dangers of Social Media

Saw the video.
### **Q4. What are your key takeaways from the above video?**
Key takeaways from the video:

* Social media is harmful to mental health and is very addictive. 
* It wastes a lot of time.
* It also plays a huge role in spreading false information a lot of time.
* Some people make the wrong usage of social media, by collecting some information about others and using that information to harass others.
* When we spend so much time on social media, which may decrease face-to-face communication skills.
* We may feel isolated and depressed.

## 6. Intentionally creating diffused mode in your life

Read the Article

## 7. Getting started on difficult tasks

Read the Article