# Browser Rendering

To understand how the browser works and what browser rendering is, We need to understand what a web browser is. In this Technical Paper, The following things are included -

* What is Web Browser and Browser Engine?
* Role of HTML, CSS, and JS
* Steps in Browser Rendering
* References

## What is Web Browser and Browser Engine?
A Web Browser is an application that accesses Websites from WWW(World Wide Web) using HTTP(Hyper Text Transfer Protocol), a set of rules for the transfer of data.  
On the other hand, Browser Engine is a core software component of every major web browser. The Browser Engine converts the HTML documents or the resources of a web page into an interactive visual representation on the user's device.

## Role of HTML, CSS, and JS
HTML(HyperText Markup Language) describes the structure of the web page semantically. HTML elements are the building blocks of HTML pages. It is often assisted by CSS(Cascading Style Sheets) and JS(Javascript) which beautifies the website as well as affects the behavior and content of Webpages respectively.

## Steps in Browser Rendering
Before going into the steps, It is necessary to understand what Rendering means.
### What is Browser Rendering?
In simple words, It can be described as the Use of HTML, CSS, and JS codes. It is a process used in Web Development that turns website code into the interactive pages users see when they visit a website. Now the steps involved are as follows -
* Navigation
* Response
* Parsing
* Dynamic Rendering
* Building Rendering Tree
* Interactivity

#### Navigation
Navigation is the first step in loading a web page. The user requests a page by entering a URL(Universal Resource Locator) into the address bar
Navigation also has substeps involved -
1. DNS Lookup: The first step of navigating to the page is finding where the assets for that page are located. The HTML page is located on the server with the IP address. The browser request for DNS Lookup and Server responds with an IP address. And then the IP will be cached for a time which in turn speeds up subsequent requests. It is supposed to be done only once per hostname.

2. TCP Handshake: Once the IP address is known, the browser sets up a connection to the server via TCP three-way handshake. The browser and the web server negotiate the parameters of the network TCP socket connection before transmitting data over HTTPS.

3. TLS Negotiation: This is another handshake that determines which cipher will be used to encrypt the communication. This verifies the server and establishes a secure connection before the transfer of data.

#### Response
Once the Connection establishes, the browser sends an initial HTTP Get request on behalf of the User to Server. The server replies with the relevant response headers and the contents of the HTML.

1. TCP Slow Start/14KB rules: The first response packet will be 14 KB. It is an algorithm that balances the speed of a network connection.

2. Congestion Control: During the transfer, the user's client confirms the delivery by returning Acknowledgements, If any acknowledgment got missed, the server registers this as missing ACKs.

#### Parsing
Parsing is the step the browser takes to turn the data it receives over the network into the DOM(Document Object Model) and CSSOM(CSS Object Model), which is used by the renderer to paint a page on the screen.

![Image](https://www.seobility.net/en/wiki/images/thumb/7/7d/HTML-rendering-tree.png/650px-HTML-rendering-tree.png)

1. Building DOM Tree: In parsing, the first step is the processing of HTML markup and building the DOM tree. It involves Tokenisation where Data is converted into bytes and bytes into characters and characters into tokens. It also involves Tree Construction. The parser parses tokenized input into the document, building up the document tree.

2. Preload Scanner: The preload scanner will parse through the content available and request high-priority resources like CSS, JavaScript, and web fonts. It will retrieve resources in the background.

3. Building CSSOM: The browser goes through each rule set in the CSS, creating a tree of nodes with parent, child, and sibling relationships based on the CSS selectors. The CSS object model is similar to the DOM.

#### Dynamic Rendering
Rendering with the help of Javascript is often termed Dynamic Rendering. It is used to create an intuitive user experience.


#### Rendering
The DOM Tree, as well as CSSOM Tree, created while Parsing are combined into a render tree which is then used to compute the layout of every visible element. Rendering steps include style, layout, painting, and, in some cases, compositing. 

After all of the Computing Styles, Computing the Geometry of each node in the tree and Painting the individual nodes to the screen. Our interface gets ready. 

### References

[What is a Browser Engine?](https://en.wikipedia.org/wiki/Browser_engine)

[Rendering](https://www.seobility.net/en/wiki/Rendering)

[Populating the page: how browsers work](https://developer.mozilla.org/en-US/docs/Web/Performance/How_browsers_work)

[What is a Web Browser?](https://en.wikipedia.org/wiki/Web_browser)
