# Grit and Growth Mindset

## 1. Grit
### **Q1. Paraphrase the video.**

The lady named "Angela Lee Duckworth" talks about her journey from a teacher to a psychologist. She tells about the students or cadets in the military academy she examined and asks the seniormost individuals present there. She found that the one who has the highest level of Grit is likely to get achievements and success. Grit is nothing but passion and preservation for very long-time goals. She also tells that she doesn't know how to increase it but tells about research where it is shown that the individuals who study this git thing and learn about how the brain works, Often fail to accept the failure. The individuals who put the effort into learning new things without losing hope are likely to have more grit than others.

**Q2. What are your key takeaways from the video to take action on?**

* I will put more effort into learning new things.
* I will increase my level of grit in myself.
* I will never lose hope and accept failure as the last result.

<br><br>

## 2. Introduction to Growth Mindset

### **Q3. Paraphrase the video.**
The narrator "Trevor Regan" talks about the concept of "Fixed Midset" and "Growth Mindset". People with a Fixed mindset are more likely to get failed and people with a Growth mindset are more likely to get success. 
People with Fixed mindsets believe that the skill can't grow. Skills are born. They often see the performance as not bad, they don't try to improve to make it better. They think efforts are useless, they see hurdles as a threat. They get discouraged easily. They also don't like to receive feedback from others. They become defensive when they listen to the feedback.
People with Growth Mindset believe that the skill can grow not born. They always try to improve their performance. They don't think efforts are useless. They like to put effort. They see hurdles as a challenge, not as a threat. They don't get discouraged at all. They also like to receive feedback from others.

### **Q4. What are your key takeaways from the video to take action on?**
* I must have a growth mindset to achieve success.
* I should believe in Skills Growth. Skills are not born.
* I must always test myself, and not be afraid of hurdles.
* I should be a responsible person in accepting feedback positively.

<br><br>

## How to stay motivated Video - The Locus Rule

### **Q5. How to stay motivated Video - The Locus Rule**

A person who has an internal locus and not an external locus is more likely to get success in every field. A person who has an external locus may succeed in one field but not in all fields. Because you will feel yourself in control of others rather than yourself. That's why having an internal locus or internally motivated is very important otherwise you will not be motivated all the time.

Internal Locus of Control means having an internally motivated mind. The people who believe in themselves get success in every field. The key point in the video is that you must have an Internal locus rather than an external one.
As discussed in the video, Children who were told they did better because they were hardworkingly performed better than others in all the activities.

<br><br>

## How to build a Growth Mindset?

### **Q6. Paraphrase the video.**
The video talks about how to build a growth mindset. The narrator first tells that to believe in ourselves. Nothing is impossible for anyone. One should always ask about his/her bad assumptions about himself/herself. One should build a curriculum to figure out the way to come out of hurdles. One should also not be afraid of challenges. It must be handled positively.


### **Q7. What are your key takeaways from the video to take action on?**

* I will believe in myself that I can do anything I want.
* I will ask myself about bad assumptions about myself.
* I will build a curriculum to resolve the issues coming my way to success.
* I will not get discouraged by failures.
* I will not be afraid of challenges.

<br><br>

## 4. Mindset - A MountBlue Warrior Reference Manual

### **Q8. What are one or more points that you want to take action on from the manual?**

- I will follow the steps required to solve problems:
    - Relax
    - Focus - What is the problem? What do I need to know to solve it?
    - Understand - Documentation, Google, Stack Overflow, GitHub * Issues, Internet
    - Code
    - Repeat
- I will not leave my code unfinished till I complete the following checklist:
    - Make it work.
    - Make it readable.
    - Make it modular.
    - Make it efficient.
- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.