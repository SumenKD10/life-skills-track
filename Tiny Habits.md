# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### **Q1. Your takeaways from the video (Minimum 5 points)**
*  The Narrator in the video talks about building new habits slowly and those habits should be tiny.
*  The narrator says "Set the habit you want to make right after something you already do."
*  To build tiny habits, Do 2 pushups right after going to the bathroom or floss just 1 tooth after brushing your teeth.
* The idea is, you have to do the activity you want to make a habit of no matter how tired you are or how much unmotivated you are.
* You should always praise yourself after successfully repeating your tiny habit to pump and motivate yourself.

## 2. Tiny Habits by BJ Fogg - Core Message
### **Q2. Your takeaways from the video in as much detail as possible**
* The narrator in the video talks about the book called "Tiny Habits" by  BJ Fogg.
* The narrator summarizes the video in the following points 
    * The universal formula for human behavior is: B = MAP where B is Behaviour, M is Motivation, A is Ability and P is Prompt.
    * BJ Fogg says that Behaviour is a result of all Motivation, Ability as well as promptness.
    * He talks about the relationship between Motivation and Ability. When Motivation is high, we can do the hardest thing possible and the easiest thing, and even if we have less motivation, we can still do that work.
    * You should shrink your Behaviour to the tiniest task possible. You can find the tiniest
version of your desired habit by either reducing the quantity or doing just the first step.
    * Your goal is to find a behavior that you can easily do in thirty seconds or less.  
    * You should identify an action prompt, which can be of three types -
        * External prompts (Alarms, Reminders)
        * Internal prompts (Sensations, Thoughts)
        * Action prompts (Activity that reminds you of the habit you wanna build)
    * Just external and internal prompts are distracting and de‐motivating. That is why we should use a formula called, " After I ____, I will ____.  "
    * You should celebrate even the tiniest success.

### **Q3. How can you use B = MAP to make making new habits easier?**
* I can make a daily routine to build new habits.
* I can then think about those habits I wanna build and break them into multiple 60 seconds tasks.
* After completing each task, I can celebrate by doing pushups or saying "Hare Krishna".
* I can apply " After I ____, I will ____.  " formula into my life. For Eg - "After I have dinner, I will take a walk" Or "After I finish coding, I will check if I followed the best practices or not"

### **Q4. Why it is important to "Shine" or Celebrate after each successful completion of a habit?**
It is very important to "Shine" as when we give ourselves a steady dose of Shine after doing the tiniest version of a habit, the motivation increases and when motivation increases, we move higher up the “action line” and can tackle harder habits.

## 3. 1% Better Every Day Video

### **Q5. Your takeaways from the video (Minimum 5 points)**
The Narrator in the video talks about the following things -
* You cannot make a rapid change in yourself. It is not possible.
* To change yourself, you have to change your habits.
* Habits are the compound interest of self-improvement.
* Good Habits make time your ally but Bad habits make time your enemy.
* There are four stages in Habit Building - 
    * **Noticing:** You should always do the premortem, track the records, and See what mistakes you are making now and how you can correct them.
    * **Wanting:** 
    Design your environment to help you make good habits easier and bad habits harder to do.
    * **Doing:** 
    Optimize for the starting line, not the finish line. 
    * **Liking** 
    To stick to a good habit you need to bring a reward into the present moment.
* Don't break the chain and never miss twice. This strategy shows progress and consistency and acts as a reward.

## 4. Book Summary of Atomic Habits

### **Q6. Write about the book's perspective on habit formation from the lens of Identity, processes, and outcomes?**

* Tiny habits have the power to make your life drastically change at a certain period.
* The narrator provides the four key insights from the book.
    * The 1% rule: You change yourself every day by 1 % through habits, if you have good habits, you will be improved but if you have bad habits, you will become worse.
   * Focus on what you have to do rather than the Goal: 
   Because there are four problems with the goals: 
        * Winners and Losers have the same goals but winners are more focused on the work.
        * Achieving a goal is just a momentary change.
        * Goals restrict our happiness.
        * Goals are at odds with long-term progress.
   * Identity change rather than outcomes: There are three layers of behavior change
        - Outcomes
        - Processes
        - Identity

    Most of us work from outcomes to identity rather than identity to an outcome
    * The 4 fundamental laws of behavior change
        1. Make it obvious
        2. Make it attractive
        3. Make it easy
        4. Make it immediately satisfying


### **Q7. Write about the book's perspective on how to make a good habit easier?**

To make good habits easier, the book suggests we make things obvious and not complicated. We should always try to make good habits attractive and easy. We should always reward ourselves even after completing the tiniest accomplishment. This encourages a person to accomplish harder good habits.


### **Q8. Write about the book's perspective on making a bad habit more difficult?**

To make a bad habit more difficult, the book suggests we create scenarios where it is hard to even attempt the bad habit. For eg, To avoid Phones before sleep, make sure you give your phone to your mom or dad so that you can never be able to reach your phone even if you want during the entire sleeping period.

## 5. Reflection:
### **Q9. Pick one habit that you would like to do more of. What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

The habit I would like to do more of is Exercise. I want to increase my fitness and focus level. I can make a daily routine and set alarms and reminders after that I can follow the rule that "After I reach my residence, I will do pushups for 60 seconds". I can celebrate after completing exercise every day by just cheering myself.

**Q10. Pick one habit that you would like to eliminate or do less of. What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

The habit that I would like to eliminate or do less of is using a smartphone at the night. I can keep my phone silent and keep it as far as possible. So that I can't reach my phone easily. I can make it look very unhealthy.