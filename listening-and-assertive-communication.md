# Listening and Active Communication
## 1. Active Listening<br><br>
### **Q1. What are the steps/strategies to do Active Listening**

* Always face the person you are talking to. Don't look here and there. Eye Contact is important.
* Don't interrupt while someone is explaining/telling something otherwise it shows you are a self-centered person. It discourages the person to ask anything.
* Always show some movement like nodding your head in Yes or responding in "Yes" or "Umm" so that the other person can know that he/she is important to you and you are carefully listening.
* Ask questions if any doubt arises. It makes the speaker believe in you and that you are listening to him/her.
* Don't judge when someone is speaking something. Let the speaker say until he/she completes. Don't conclude soon.
* Analyse the non-verbal gestures too. While talking, It is necessary to see the face as well as hand gestures.<br><br>

## 2. Reflective Listening <br>
### **Q2. According to Fisher's model, what are the key points of Reflective Listening?**

* Listen more and Talk less.

* Clarify what the other has said, Don't ask questions or tell what you feel.

* Try to understand the feelings inside the conversation.

* Share your acceptance and empathy, Don't show fake concern.<br><br>

## 3. Reflection
### **Q3. What are the obstacles in your listening process?**

* **Surrounding Distraction** <br>
    I can't concentrate on the person if something is happening in the nearby surroundings.
* **Uncomfortable with Stranger** <br>
    I always doubt a person if he/she is unknown to me. Can't able to focus on the facts he/she speaks.
* **Boring if listened again** <br>
    I feel bored if I listen to the same thing again and again, due to which I miss important facts spoken out in between.

### **Q4. What can you do to improve your listening?**
* I will start listening more and more and stop judging without listening to the person even if he/she is a stranger.
* I will focus more on the person rather than the unnecessary activities happening in the surrounding.
* I will practice listening more and more through Reflective Listening Principles.
<br><br>

## 4. Types of Communication
### **Q5. When do you switch to a Passive communication style in your day-to-day life?**
* Whenever I talk to any elder or someone more mature than me, I switch to a Passive Communication Style
* Whenever I find myself in a difficult situation, When I don't know what to do.
* Whenever I find myself weak in front of the second person in one or the other way.
<br>

### **Q6. When do you switch to Aggressive communication styles in your day-to-day life?**
* Whenever I find someone saying annoying to me.
* When I don't get the reward/output for my hard work.
* When I see someone doing bad to someone.
<br>

### **Q7. When do you switch to Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day-to-day life?**
* When I see someone bootlicking.
* When I see someone testing others for selfish reasons.
* When I am with my brother/close friends.
<br>

### **Q8. How can you make your communication assertive? You can analyze the videos and then think about what steps you can apply in your own life.**
* I will Communicate with different people and practice more and more.
* I will make points before the meeting itself about what to say and what not to say.
* I will take care of my body language during the meeting.
* I will say "No" if I find anything uncomfortable.