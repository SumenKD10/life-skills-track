# Good Practices for Software Development

## 1. Gathering requirements

Read the Article

## 2. Always over-communicate: Some scenarios

Read the Article

## 3. Stuck? Ask questions

Read the Article

## 4. Get to know your teammates

Read the Article

## 5. Be aware and mindful of other team members

Read the Article 

## 6. Doing things with 100% involvement

Read the Article

## Review Questions

### **Q1. What is your one major takeaway from each one of the 6 sections? So 6 points in total.**
* We should always make note of what is being discussed in the meeting and take frequent feedback and clarity to ensure that we are on track as everyone others.

* We should always try not to have a communication gap. If one can't receive a call due to some emergency, one should receive the call and inform about how long the caller has to wait. One should always be informed of what has gone wrong during the project implementation and what will be the new deadline.

* One should always ask a proper question and not a brief one. One should always try to give the reasoning, steps taken till now, screenshots, and code snippets used to the respective team members. If needed, you different tools available on the internet like loom.

* We should always try to establish good communication between team members. This helps in improving communication. Knowing their time schedules will help you to ask questions when they are free.

* Always try to ask the question when the person you are asking is free. We should always try not to become a hurdle in their work.

* We should always try to balance out our life, we should work while working and play while playing. We should always try to avoid social media during working hours and always try to do exercise to keep ourselves healthy and focused.


### **Q2.Which area do you think you need to improve on? What are your ideas to make progress in that area?**

I think I need to improve on receiving calls on time. I am pretty at it. I have to improve in this area. I also don't like to carry Notebook and Pen while going to a meeting. I also need to take care of that. I also have to do Good Time Management, so that I don't have to the work at the last moment.
