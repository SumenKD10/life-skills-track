# Energy Management

## 1. Manage Energy Not Time
### **Q1. What are the activities you do that make you relax - Calm quadrant?**
- Listening to Soothing Music
- Going to Temple
- Watching Entertainment Videos
- Doing Meditation
- Talking to Family Members

### **Q2. When do you find getting into the Stress quadrant?**
- When I get a task of which I have no Idea.
- When the Exam comes.
- When Code gives an error of losing a Parenthesis.
- When I think about my Future.

### **Q3. How do you understand if you are in the Excitement quadrant?**
- When I go to a new Place.
- When I am doing Coding.
- When I try a new Dish or Dessert.
- When I watch Anime.

## 2. Strategies to deal with Stress

Read the things written.

## 3. Meditation

Read the articles and watched the video.

## 4. Sleep is your superpower

### **Q4. Paraphrase the video**
* Sleep is a very essential daily routine.
* Sleep impacts our life in many ways.
* Sleep is very essential to memorize things and making that capability.
* Sleep is also necessary for the development of Sexual Organs.
* It is also important to know that less sleep means less life expectancy.
* WHO has classified any form of night-time shift work as probable-carcinogenic, because of disruption of sleep rhythm.
* Lack of sleep destroys natural immune cells.
* Less Sleep can even cause Prostate Cancer.
* Don't learn anything in bed, make some difference.
* Always prioritize sleep over anything.
* People who sleep less have difficulty finding mental peace.

### **Q5. What are some ideas that you can implement to sleep better?**
* Avoiding Mobiles/Laptops or anything after having dinner.
* Keeping the body calm and avoiding negativity.
* Doing meditation.
* Read Books that are boring at the night.

## 5. Brain-Changing Benefits of Exercise

### **Q6. Paraphrase the video**
* The lady in the video talks about the benefits of exercise.
* She tells Exercise can enhance your learning ability.
* Helps to maintain focus and attention for long period.
* Exercise can boost energy and mood.
* Exercise can enhance the Protection of the brain.

### **Q7. What are some steps you can take to exercise more?**

* Exercise daily for 30 minutes.
* Check your weight on regular basis.
* Drink more water and keep yourself active.
* Walk here are there when getting confused about anything.
* Play some games and sports.