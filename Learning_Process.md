# Learning Process
## 1. How to Learn Faster with the Feynman Technique
### **Q1. What is the Feynman Technique?**

* If you want to learn something, try to explain or at least pretend to do so. This makes the man generate curiosity regarding the topic and learn it with more understanding.
* Try to explain in simple language. Explain as if you are explaining to a kid. Be ready for his/her why?
* Understand your weaker or shady area. Note down in which area you feel unconfident and focus more on it.
* Identify what technical words you used in your explanation and know how to explain those terms in simple language too.
<br><br>

### **Q2. What are the different ways to implement this technique in your learning process?**
* I can ask the person sitting adjacent to me if he/she has any doubts and I can explain it in my simple language.
* I can revise the topics which I feel less confident about.
* I can talk to kids around in my free time and tell some technical terms in easy language.
* I can identify the topics in which I feel I am not able to explain further and come up with an explanation as easily as possible.

<br><br>

## 2. Learning How to Learn TED talk by Barbara Oakley <br>
### **Q3. Paraphrasing the Video**

* The orator talks about two different modes that are Focused and Relaxed mode.
* Anyone who wants to learn something has to switch to Focused Mode.
* Use different techniques to learn anything accordingly.
* To deal with procrastination, Set a timer and promise yourself to work or learn with Focus during this time.
* Slow learners don't have to think they are less as they have much more creative skills but they have to work hard by unlocking the bond of their interest area.
* Don't be afraid of any tests.
* After learning anything, Recall it and practice it more and more.
* Make flashcards to improve your skills and generate interest in that particular area.
* Don't just follow your passion, Broaden your passion. 

### **Q4. What are some of the steps that you can take to improve your learning process?**
* I can use different techniques to be in focused mode while learning anything, Like Drinking water, Doing exercise, etc.
* I can use timers to deal with procrastination.
* I can stop being scared of tests.
* I can recall the learned things.
* I can make use of flash cards to learn and generate interest in that particular area.
* I can broaden my passion area by exploring new things.
<br><br>

## 3. Learn Anything in 20 hours
### **Q5. Paraphrasing the Video**
* The main obstacle in the art of learning a new skill is not Intellectual Ability. It is all mind game, it is all emotion.
* Practice for 20 hours on whatever you want to learn.
* Remove distractions while learning.
* Break the thing you want to learn into smaller units and then learn.
* Learn enough to self-correct.

### **Q6. What are some of the steps that you can while approaching a new topic?**
* I can break down the topic into smaller sub-topics and then learn.
* I can learn the important things to learn just enough. Not going too deep.
* I can remove distractions while learning a new topic.
* I can practice any new topic for 20 hours to become confident in it.
* I can build myself emotionally strong.