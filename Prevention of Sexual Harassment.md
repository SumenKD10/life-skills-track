# Prevention of Sexual Harassment

## **Q1. What kinds of behavior cause sexual harassment?**
Sexual Harassment can be classified into three different types -
* **Physical :** <br>
 Physical harassment can be an aggressive attitude in a sexual manner. It can be blocking Movement, inappropriate touching, and rubbing. Sexual harassment can be inappropriate gestures as well as staring too.

* **Visual :**<br>
Obscene Drawings and Posters, Screensavers, Wallpapers, Emails, etc are counted as Visual Harassment.

* **Verbal** <br>
Making sexual jokes, gender-related jokes, commenting on body looks, asking out someone even after being rejected many times, and commenting on clothing style are counted as Verbal harassment.

## **Q2. What would you do in case you face or witness any incident or repeated incidents of such behavior?**
Firstly, I will check if the person who is doing harassment in any form whether that particular person doing it intentionally or unintentionally. If the person doing it unintentionally, I will tell him to stop doing whatever he/she is doing firmly in an assertive manner. If he/she doesn't stop, I will inform the higher authorities.

Secondly, If things are self hand able, I will talk to him/her personally, If he/she repeats, I will talk to friends and family about this issue and will ask for suggestions as to what to do. I will also talk to Sexual Harassment Issue Undertaker.